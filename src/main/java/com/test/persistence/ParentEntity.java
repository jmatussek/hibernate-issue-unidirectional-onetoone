package com.test.persistence;


import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "Parent")
public class ParentEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue
    @Type(type = "org.hibernate.type.UUIDCharType")
    public UUID id;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "id")
    public ChildEntity child;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ChildEntity getChild() {
        return child;
    }

    public void setChild(ChildEntity child) {
        this.child = child;
    }
}
