package com.test;

import com.test.hibernate.HibernateUtil;
import com.test.persistence.ChildEntity;
import com.test.persistence.ParentEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class App {

    public static void main(String[] args) {
        ParentEntity parent = new ParentEntity();

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(parent);
            ChildEntity child = new ChildEntity();
            child.setId(parent.getId());
            session.save(child);

            parent.setChild(child);
            session.update(parent);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.delete(parent);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
}
